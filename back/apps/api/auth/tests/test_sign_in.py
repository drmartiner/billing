from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.utils import get_user_by_session


class SignInTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-in')

    def setUp(self):
        super(SignInTest, self).setUp()

        self.post_data = {
            'username': self.user.username,
            'password': self.password,
        }

    def test_succeed(self):
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.user.pk, user.pk)

    def test_username_uppercase(self):
        self.post_data['username'] = self.post_data['username'].upper()

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.user.pk, user.pk)

    def test_wrong_username(self):
        self.post_data['username'] = 'wrong'

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus401(response)

    def test_wrong_password(self):
        self.post_data['password'] = 'wrong-password'

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus401(response)

    def test_wrong_data(self):
        self.post_data['username'] = 'wrong (*&*^%$#@ username'
        del self.post_data['password']

        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus400(response)
