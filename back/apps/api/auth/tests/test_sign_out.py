from django.urls import reverse

from apps.api.base_test import BaseApiTest


class SignOutTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-out')

    def test_succeed(self):
        self.client.force_login(self.user)

        response = self.client.post(self.url)
        self.assertResponseStatus202(response)

    def test_un_auth(self):
        response = self.client.post(self.url)
        self.assertResponseStatus403(response)
