from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.utils import get_user_by_session
from apps.common.fields import CurrencyField
from apps.common.utils import get_object_or_none
from apps.users.models import User


class SignUpTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up')

    def setUp(self):
        super(SignUpTest, self).setUp()

        self.post_data = {
            'username': 'new_user_name',
            'password': '123',
            'currency': CurrencyField.CAD,
            'first_name': 'Alex',
            'last_name': 'Petrov',
            'country': 'Russia',
            'city': 'Moscow',
        }

    def test_succeed(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        user: User = get_object_or_none(User, username__iexact=self.post_data['username'])
        self.assertIsNotNone(user)
        self.assertTrue(user.is_active)

        is_right = user.check_password(self.post_data['password'])
        self.assertTrue(is_right)

        self.assertIn('sessionid', response.client.cookies)
        user = get_user_by_session(response.client.cookies['sessionid'].value)
        self.assertEquals(self.post_data['username'], user.username)
        self.assertEquals(self.post_data['currency'], user.currency)
        self.assertEquals(self.post_data['first_name'], user.first_name)
        self.assertEquals(self.post_data['last_name'], user.last_name)
        self.assertEquals(self.post_data['country'], user.country)
        self.assertEquals(self.post_data['city'], user.city)

    def test_already_exists(self):
        self.post_data['username'] = self.username

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus400(response)

        count = User.objects.filter(username=self.post_data['username']).count()
        self.assertEquals(count, 1)
