from django.contrib.auth import login, logout
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.common.utils import get_object_or_none
from apps.users.models import User
from . import serializers


class SignUpView(APIView):
    messages_map = {
        202: _('User was created.'),
        409: _('Username is already exists.'),
    }
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = User.objects.create(is_active=True, **serializer.validated_data)
        user.set_password(serializer.data['password'])
        user.save()

        login(request, user, 'django.contrib.auth.backends.ModelBackend')

        return Response(status=status.HTTP_201_CREATED)


class SignInView(APIView):
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, username__iexact=serializer.data['username'])
        if not user:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        is_right = user.check_password(serializer.data['password'])
        if not is_right:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        login(request, user, 'django.contrib.auth.backends.ModelBackend')

        return Response(status=status.HTTP_202_ACCEPTED)


class SignOutView(APIView):
    def post(self, request, *args, **kwargs):
        logout(request)

        return Response(status=status.HTTP_202_ACCEPTED)
