from django.urls import path, include

urlpatterns = [
    path('auth/', include(('apps.api.auth.urls', 'apps.api'), namespace='auth')),
    path('docs/', include(('apps.api.docs.urls', 'apps.api'), namespace='docs')),
    path('user/', include(('apps.api.user.urls', 'apps.api'), namespace='user')),
    path('wallet/', include(('apps.api.wallet.urls', 'apps.api'), namespace='wallet')),
]
