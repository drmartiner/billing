from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.user import serializers


class ReadSelfProfileTest(BaseApiTest):
    url = reverse('api_v1:user:self-profile')

    def test_read(self):
        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.SelfProfileRetrieveSerializer, self.user, response)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
