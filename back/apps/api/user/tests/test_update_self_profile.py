from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.common.fields import CurrencyField
from apps.api.user import serializers


class UpdateSelfProfileTest(BaseApiTest):
    url = reverse('api_v1:user:self-profile')

    def setUp(self):
        super(UpdateSelfProfileTest, self).setUp()

        self.post_data = {
            'username': 'new_user_name',
            'balance': 100500.1,
            'currency': CurrencyField.CAD,
            'first_name': 'New first name',
            'last_name': 'New last name',
            'country': 'New country',
            'city': 'New city',
        }

    def test_update(self):
        self.client.force_login(self.user)

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()
        self.assertRetrieveInResponse(serializers.SelfProfileRetrieveSerializer, self.user, response)

        self.reload_user_instance()
        self.assertEquals(self.user.first_name, self.post_data['first_name'])
        self.assertEquals(self.user.last_name, self.post_data['last_name'])
        self.assertEquals(self.user.city, self.post_data['city'])
        self.assertEquals(self.user.country, self.post_data['country'])
        self.assertNotEquals(self.user.username, self.post_data['username'])
        self.assertNotEquals(self.user.balance, self.post_data['balance'])
        self.assertNotEquals(self.user.currency, self.post_data['currency'])

    def test_un_auth(self):
        response = self.client.post(self.url, self.post_data)
        self.assertResponseStatus403(response)
