from django.urls import path

from . import views

urlpatterns = [
    path('self/', views.SelfProfileView.as_view(), name='self-profile'),
]
