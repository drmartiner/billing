from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.users.models import User
from . import serializers


class SelfProfileView(APIView):
    def get(self, request, *args, **kwargs):
        user = request.user
        serializer = serializers.SelfProfileRetrieveSerializer(instance=user)

        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        serializer = serializers.SelfProfileUpdateSerializer(data=request.data, instance=request.user)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        user = User.objects.get(pk=request.user.pk)
        serializer = serializers.SelfProfileRetrieveSerializer(instance=user)

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
