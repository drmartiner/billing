from rest_framework import serializers

from apps.users.models import User


class UserField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        return User.objects.exclude(pk=self.context['request'].user.pk)
