from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers
from apps.wallet.models import Course


class GetCourseTest(BaseApiTest):
    url = reverse('api_v1:wallet:course')

    def test_read(self):
        course: Course = G(Course)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.CourseSerializer, course, response)
