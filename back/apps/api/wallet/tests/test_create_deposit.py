from django.urls import reverse

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers
from apps.wallet.models import Deposit


class CreateDepositTest(BaseApiTest):
    url = reverse('api_v1:wallet:deposit-create')
    post_data = {'amount': 100500.}

    def test_read(self):
        self.client.force_login(self.user)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        deposit = Deposit.objects.first()
        self.assertIsNotNone(deposit)

        self.assertRetrieveInResponse(serializers.DetailDepositSerializer, deposit, response, self.user)

        self.assertEquals(deposit.amount, self.post_data['amount'])
        self.assertEquals(deposit.user.pk, self.user.pk)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
