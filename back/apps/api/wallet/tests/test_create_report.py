from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers
from apps.users.models import User
from apps.wallet.models import Report, Transfer


class CreateReportTest(BaseApiTest):
    url = reverse('api_v1:wallet:report')

    def setUp(self):
        super(CreateReportTest, self).setUp()

        self.user_for = G(User)

        self.date_from = timezone.now() - timedelta(days=1)
        self.date_to = timezone.now() - timedelta(days=3)
        self.post_data = {
            'type': Report.TYPE.CSV,
            'user_for': self.user_for.pk,
            'date_from': self.date_from.isoformat(),
            'date_to': self.date_to.isoformat(),
        }

    def test_succeed(self):
        for i in range(3):
            G(Transfer, user_from=self.user_for, created=timezone.now() - timedelta(days=2))
        G(Transfer, created=timezone.now() - timedelta(days=2))

        self.client.force_login(self.user)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        report = Report.objects.last()
        self.assertIsNotNone(report)

        self.assertRetrieveInResponse(serializers.ReportSerializer, report, response, self.user)

        self.assertEquals(report.owner.pk, self.user.pk)
        self.assertEquals(report.user_for.pk, self.user_for.pk)
        self.assertEquals(report.type, self.post_data['type'])
        self.assertEquals(report.date_from.isoformat(), self.date_from.isoformat())
        self.assertEquals(report.date_to.isoformat(), self.date_to.isoformat())
        self.assertFalse(bool(report.file))
        self.assertFalse(report.is_done)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)
