from django.urls import reverse
from django_dynamic_fixture import G

from apps.api.base_test import BaseApiTest
from apps.api.wallet import serializers
from apps.common.fields import CurrencyField
from apps.users.models import User
from apps.wallet.models import Transfer, Course


class CreateTransferTest(BaseApiTest):
    url = reverse('api_v1:wallet:transfer-create')

    def setUp(self):
        super(CreateTransferTest, self).setUp()

        self.course = G(Course, eur=1, cad=1, cny=1)

        self.user_to = G(User, currency=CurrencyField.CAD)
        self.post_data = {
            'amount': 10.,
            'user_to': self.user_to.pk,
        }

    def test_succeed(self):
        self.update_user_instance(balance=100.)
        self.client.force_login(self.user)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        transfer = Transfer.objects.first()
        self.assertIsNotNone(transfer)

        self.assertRetrieveInResponse(serializers.DetailTransferSerializer, transfer, response, self.user)

        self.assertEquals(transfer.amount, self.post_data['amount'])
        self.assertEquals(transfer.user_from.pk, self.user.pk)
        self.assertEquals(transfer.user_to.pk, self.user_to.pk)
        self.assertEquals(transfer.course.pk, self.course.pk)

    def test_concurrency(self):
        balance = 100.
        self.update_user_instance(balance=balance)
        self.client.force_login(self.user)

        self.post_data['amount'] = balance / 2
        self.run_concurrency(self.client.post, 5, [{'path': self.url, 'data': self.post_data} for i in range(5)])

        self.reload_user_instance()
        self.assertEquals(self.user.balance, 0.0)

    def test_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)

        transfer = Transfer.objects.first()
        self.assertIsNone(transfer)
