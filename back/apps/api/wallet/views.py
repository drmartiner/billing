from _decimal import Decimal

from django.http import HttpResponseGone
from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.api.renders import BinaryFileRenderer
from apps.wallet.tasks import make_report
from apps.wallet.models import Deposit, Course, Report
from apps.wallet.wallet import Wallet
from . import serializers


class CourseView(generics.RetrieveAPIView):
    permission_classes = []
    serializer_class = serializers.CourseSerializer

    def get_object(self):
        return Course.objects.last()


class BalanceView(generics.RetrieveAPIView):
    serializer_class = serializers.BalanceSerializer

    def get_object(self):
        return self.request.user


class CreateDepositView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = serializers.CreateDepositSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        deposit: Deposit = Wallet.deposit(request.user.id, serializer.data['amount'])

        serializer = serializers.DetailDepositSerializer(instance=deposit)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CreateTransferView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = serializers.CreateTransferSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)

        transfer = Wallet.transfer(
            request.user.id,
            serializer.data['user_to'],
            Decimal(serializer.data['amount'])
        )

        serializer = serializers.DetailTransferSerializer(instance=transfer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CreateReportView(generics.ListCreateAPIView):
    serializer_class = serializers.ReportSerializer

    def get_queryset(self):
        return Report.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        report = serializer.save(owner=self.request.user)
        make_report.delay(report.pk)


class DetailReportView(generics.RetrieveAPIView):
    renderer_classes = [BinaryFileRenderer]
    serializer_class = serializers.ReportSerializer

    def get_queryset(self):
        return Report.objects.filter(owner=self.request.user)

    def perform_content_negotiation(self, request, force=False):
        report: Report = self.get_object()

        if not report.is_done:
            return HttpResponseGone()

        filename = f'billing-report-{report.uuid}.{report.type}'
        with open(report.file.path, 'rb') as file:
            return Response(
                file.read(),
                headers={'Content-Disposition': f'attachment; filename="{filename}"'},
                content_type=f'application/{report.type}'
            )
