from apps.common.base_test import BaseTest

from apps.common.fields import CurrencyField


class CurrencyFieldTest(BaseTest):
    def test_default_kwargs(self):
        field = CurrencyField()

        self.assertEqual(field.default, CurrencyField.USD)
        self.assertEqual(field.choices, CurrencyField.CHOICES)

    def test_check_allowed_symbols(self):
        try:
            CurrencyField.check_symbol_allowed(CurrencyField.USD)
        except ValueError:
            self.fail('CurrencyField.check_symbol_allowed was failed')
        except Exception as e:
            raise e

        with self.assertRaises(ValueError) as cm:
            CurrencyField.check_symbol_allowed('unknown')
