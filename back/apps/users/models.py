import logging

from django.contrib.auth.models import AbstractUser
from django.db import models

from apps.common.fields import CurrencyField

__all__ = ['User']

logger = logging.getLogger('django.request')


class User(AbstractUser):
    REQUIRED_FIELDS: tuple = ('currency', 'email')

    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    currency = CurrencyField()

    country = models.CharField(max_length=32, blank=True, null=True)
    city = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        app_label = 'users'

    def __str__(self):
        return self.email
