from django.contrib import admin

from . import models


@admin.register(models.Deposit)
class DepositAdmin(admin.ModelAdmin):
    raw_id_fields = ['user']
    list_display = ['id', 'user', 'amount', 'created']
    search_fields = ['user__username']
    list_filter = ['created']


@admin.register(models.Transfer)
class TransferAdmin(admin.ModelAdmin):
    raw_id_fields = ['user_from', 'user_to', 'course']
    list_display = ['id', 'user_from', 'user_to', 'amount', 'created']
    search_fields = ['user_from__username', 'user_to__username']
    list_filter = ['created']


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ['id', 'eur', 'cad', 'cny', 'created']
    list_filter = ['created']


@admin.register(models.Report)
class ReportAdmin(admin.ModelAdmin):
    raw_id_fields = ['owner', 'user_for']
    list_display = ['uuid', 'owner', 'type', 'user_for', 'is_done', 'created']
    list_filter = ['is_done', 'type', 'created']
    search_fields = ['owner__username', 'user_for__username']
    readonly_fields = ['uuid', 'created']
    radio_fields = {'type': admin.HORIZONTAL}

    fieldsets = (
        ('Main', {'fields': ('uuid', 'owner', 'is_done', 'file', 'created')}),
        ('Params', {'fields': ('type', 'user_for', 'date_from', 'date_to')}),
    )
