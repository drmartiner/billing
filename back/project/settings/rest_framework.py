from .env import env

JSON_EDITOR = True

REST_FRAMEWORK = {
    'PAGE_SIZE': 20,
    'UPLOADED_FILES_USE_URL': env('ROLE') != 'test',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_RENDERER_CLASSES': [
        'apps.api.renders.CustomProtocolJSONRenderer',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'apps.api.authentications.CsrfExemptSessionAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ],
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}
